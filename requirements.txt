ConcurrentLogHandler==0.9.1
Django==1.11.18
django-debug-toolbar==1.9.1
django-extensions==2.0.5
django-guardian==1.4.9
django-model-utils==3.1.1
django-ses
django-silk==3.0.1
django-twilio==0.9.0
django-user-sessions==1.6.0
djangorestframework==3.9.0
dnspython==1.15.0
flake8==3.5.0
gunicorn[gevent]==19.9.0
html5lib==1.0.1
ipdb==0.10.2
ipython==5.5.0
# Temporary pin of lvalert to git.ligo.org
git+https://git.ligo.org/lscsoft/lvalert.git@13a9e40649eeb2a14369fcee950cd49154f195ad#egg=ligo-lvalert
#ligo-lvalert==1.5.5
ligo-lvalert-overseer==0.1.3
lxml==4.2.0
matplotlib==2.0.0
mock==2.0.0
mysql-python==1.2.5
packaging==17.1
phonenumbers==8.8.11
python-ldap==3.1.0
python-memcached==1.59
scipy==1.2.1
service_identity==17.0.0
simplejson==3.15.0
Sphinx==1.7.0
twilio==6.10.3
# Fixed at old versions to prevent bugs:
# https://github.com/fritzy/SleekXMPP/issues/477
# https://github.com/etingof/pyasn1/issues/112
pyasn1==0.3.6
pyasn1-modules==0.1.5
pytz==2018.9
